// Posner Task:
// Example adapted from the PsychoPy Online Video Tutorial:
// https://www.youtube.com/watch?v=ZQd2QEK_Gn4
// Demo Script written as an example for Tübingen Workshop on Online Experiments

function genVpNum() {
  // Participant number based on time
  "use strict";
  let num = new Date();
  num = num.getTime();
  jsPsych.data.addProperties({ vpNum: num });
  return num;
}

const vpNum = genVpNum();

// Can't use PHP code on Pavlovia?
// Two options
// Option 1: Assign version randomly based on mod of time
const version1 = getVersionNumber(vpNum, 4);
console.log(version1);

// Option 2: Consequtive participant numbers provided by the following link
// https://moryscarter.com/vespr/pavlovia.php
const version2 = getVersionNumber(jsPsych.data.urlVariables().participant, 4);
console.log(version2);

////////////////////////////////////////////////////////////////////////
//                      Experiment Instructions                       //
////////////////////////////////////////////////////////////////////////
const welcome = {
  type: "html-keyboard-response",
  stimulus: `<H1>Welcome to Pavlovia and jsPsych!<br><br>
    Press the spacebar to continue!</H1>`,
  post_trial_gap: prms.waitDur,
};

////////////////////////////////////////////////////////////////////////
//                        Pavlovia Interaction                        //
////////////////////////////////////////////////////////////////////////
const pavolvia_init = {
  type: "pavlovia",
  command: "init",
};

const pavolvia_finish = {
  type: "pavlovia",
  command: "finish",
};

////////////////////////////////////////////////////////////////////////
//                    Generate and run experiment                     //
////////////////////////////////////////////////////////////////////////
function genExpSeq() {
  "use strict";

  let exp = [];
  exp.push(pavolvia_init);
  exp.push(welcome);
  exp.push(pavolvia_finish);

  return exp;
}
const EXP = genExpSeq();

jsPsych.init({
  timeline: EXP,
  show_progress_bar: false,
  preload_images: imgs,
});
